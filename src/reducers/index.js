import { combineReducers } from "redux";
import pokemonListReducer from './pokemonListReducer';
import pokemonReducer from './pokemonReducer';
import abilityReducer from './abilityReducer';
import teamReducer from './teamReducer';

export default combineReducers({
    pokemonListReducer,
    pokemonReducer,
    abilityReducer,
    teamReducer
});