import { ADD_TO_TEAM, REMOVE_FROM_TEAM } from "../types/teamTypes";

const INITIAL_STATE = {
    team: [],
    isTeamFull: false
}

const teamReducer = (state = INITIAL_STATE, { type, payload }) => {
    switch (type) {
        case ADD_TO_TEAM:
            return {
                ...state,
                team: [...state.team, payload],
                isTeamFull: state.team.length === 5
            }
        case REMOVE_FROM_TEAM:

            return {
                ...state,
                team: state.team.filter((t, i) => i !== payload),
                isTeamFull: false
            }
        default:
            return state;
    }
}

export default teamReducer;