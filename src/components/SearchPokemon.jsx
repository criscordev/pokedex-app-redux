import React, { useRef } from 'react';
import { connect } from 'react-redux';
import * as pokemonListActions from '../actions/pokemonListActions';

const SearchPokemon = ({ filterList }) => {
    const searchInput = useRef(null);
    const onSearch = () => {
        filterList(searchInput.current.value);
    }
    return (<div className='card card-custom align-middle'>
        <div className='row pl-5 pt-5'>
            <h3 className="panel-title">Search Pokemon</h3>
        </div>
        <div className='container align-middle pt-4 pl-5 pr-5'>
            <div className="input-group">
                <input ref={searchInput} onChange={onSearch} type="text" className="form-control" placeholder="Pokemon" />
                <div className="input-group-prepend">
                    <span className="input-group-text">?</span>
                </div>
            </div>
        </div>
    </div>)
}

export default connect(null, pokemonListActions)(SearchPokemon);