import React, { useState } from 'react';
import useInterval from '../hooks/useInterval';
import '../../styles/rainbowBtn.css';
import '../../styles/pokemonTypesColors.css';
const PokeImage = ({ sprites, name, shinyButton = true, types }) => {
    const [state, setState] = useState(true);
    const [shiny, setShiny] = useState(false);
    useInterval(() => { setState(!state); }, 1000);

    const toggleShiny = () => setShiny(!shiny);

    return (<div>
        {
            types.map((type, i) => <span key={i} className={`badge ${type.type.name} ml-1`}>{type.type.name}</span>)
        }
        <br></br>
        {
            shiny ? state ? <img src={sprites.back_shiny} alt="back_shiny" /> : <img src={sprites.front_shiny} alt="front_shiny" /> :
                state ? <img src={sprites.back_default} alt="back_default" /> : <img src={sprites.front_default} alt="front_default" />
        }
        <p className='align-middle'> {name.toUpperCase()} </p>
        {shinyButton && <button className='btn-primary' onClick={toggleShiny}>{shiny ? 'normal' : 'shiny!'}</button>}

    </div>);
}

export default PokeImage;