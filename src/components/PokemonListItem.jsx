import React from 'react';
import { connect } from 'react-redux';
import * as pokemonActions from '../actions/pokemonActions';
const PokemonListItem = ({ pokemon, index, getPokemon, indexSelected }) => {

    const handleClick = () => {
        if (index !== indexSelected) getPokemon(pokemon.name, index);
    }

    return (<button onClick={handleClick} type="button" className={`list-group-item list-group-item-action d-flex justify-content-between align-items-center ${index === indexSelected ? 'active border-bot-active' : 'border-bot'} `}>
        <span className="badge badge-primary badge-pill">{pokemon.url.substring(34,50).replace('/','')}</span>
        <div>
            {`${pokemon.name} `}
        </div>
    </button>);
}

const mapStateToProps = ({ pokemonListReducer }) => {
    return pokemonListReducer;
}

export default connect(mapStateToProps, pokemonActions)(PokemonListItem);