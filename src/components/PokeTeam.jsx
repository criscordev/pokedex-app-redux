import React from 'react';
import { connect } from 'react-redux';
import PokeImage from './shared/PokeImage';
import * as teamActions from '../actions/teamActions';
import InitialCard from './shared/InitialCard';
const PokeTeam = ({ team, removeFromTeam }) => {
    const remove = (i) => {
        removeFromTeam(i);
    }

    return (<div className='align-middle to-back'>

        <div className='card card-custom align-middle'>
            <div className='row pl-5 pt-2'>
                <h3 className="panel-title">Team</h3> <p>{team.length}/6</p>
            </div>
            {team.length > 0 ?
                <ul className="list-group list-group-horizontal align-items-center">
                    {team.map((pokemon, i) =>
                        <li key={i} className="list-group-item align-middle">
                            <div className="container">
                                <div className='row '>
                                    <div className='d-flex justify-content-between '>
                                        <PokeImage
                                            sprites={pokemon.sprites}
                                            name={pokemon.name}
                                            shinyButton={false}
                                            types={pokemon.types}
                                        />
                                        <button type='button' onClick={() => remove(i)} className="btn-custom btn badge badge-pill">x</button>
                                    </div>
                                </div>
                            </div>
                        </li>)}
                </ul>
                : <InitialCard message='No one chosen' />}
        </div>
    </div>);
}
const mapStateToProps = ({ teamReducer }) => {
    return teamReducer;
}
export default connect(mapStateToProps, teamActions)(PokeTeam);