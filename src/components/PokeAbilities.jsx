import React, { Fragment, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import Card from './shared/Card';
import '../styles/dropdown.css'
import AbilityInfo from './AbilityInfo';
import * as abilityActions from '../actions/abilityActions';
import InitialCard from './shared/InitialCard';
import { VisibilityOutlined } from '@material-ui/icons';

const PokeAbilities = ({ pokemon, error, loading, getPokemon }) => {

    const [selected, setSelected] = useState(false);
    const [key, setKey] = useState(null);

    useEffect(() => {
        setSelected(false);
    }, [pokemon]);

    const onClick = (i, url) => {

        if (key === i && selected) {
            setSelected(false);
        } else {
            getPokemon(url);
            setSelected(true);
            setKey(i);
        }
    }

    const closeInfo = () => {
        setSelected(false);
    }

    const pokemonAbilities = pokemon ? <Fragment>
        <div className="panel panel-primary col-md-12 col-sm-12 container pb-2 " id="result_panel">
            <div className="panel-heading"><h3 className="panel-title">Abilities</h3>
            </div>
            <div className="panel-body">
                <div className="list-group">
                    {pokemon.abilities.map((d, i) =>
                        <button onClick={() => onClick(i, d.ability.url)} type="button" className='list-group-item list-group-item-action d-flex justify-content-between' key={i}>
                            {d.ability.name}
                            <VisibilityOutlined />
                        </button>)}
                </div>
            </div>
            {selected && <AbilityInfo closeInfo={closeInfo} />}
        </div>
    </Fragment> : <InitialCard message='Select a Pokemon!' />;

    return (<Card loading={loading} error={error}>
        {pokemonAbilities}
    </Card>)
}

const mapStateToProps = ({ pokemonReducer }) => {
    return pokemonReducer;
}

export default connect(mapStateToProps, abilityActions)(PokeAbilities);