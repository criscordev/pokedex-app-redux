import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import PokemonList from './PokemonList';
import * as PokemonListActions from '../actions/pokemonListActions';
import PokemonMainInfo from './PokemonMainInfo';
import Header from './Header';
import PokeAbilities from './PokeAbilities';
import PokeTeam from './PokeTeam';
import SearchPokemon from './SearchPokemon';
const App = ({ getAllList }) => {

  getAllList();

  return (
    <div className="App pb-5">
      <Header />
      <div className="jumbotron jumbotron-fluid banner-custom">
        <div className="container">
          <h1 className="display-5 goldman-font">Pokedex app by Cristian Cornejo</h1>
          <p className="lead">This app was maked using React, Redux and Bootstrap.</p>
        </div>
      </div>
      <div className='container goldman-font'>
        <div className='row '>
          <div className='col-md-4 col-sm-12 pt-2'>
            <PokemonList />
          </div>
          <div className='col-md-8 col-sm-12 pt-2'>
            <div className='row'>
              <div className='col-sm-12'>
                <SearchPokemon />
              </div>
            </div>
            <div className='row pt-3'>
              <div className='col-md-6 col-sm-12'>
                <PokemonMainInfo />
              </div>
              <div className='col-md-6 col-sm-12'>
                <PokeAbilities />
              </div>

            </div>
          </div>
          <div className='col-sm-12 pt-3'>
            <PokeTeam />
          </div>
        </div>

      </div>

    </div>
  );
}

export default connect(null, PokemonListActions)(App);
